﻿using System.Collections.Generic;
using CloudDriveAggregator.Core.Models;

namespace CloudDriveAggregator.Core.Interfaces
{
    public abstract class AdapterBase : IAdapter
    {
        public virtual File GetFileByUrl(string url)
        {
            // todo: implement this, using ur favorite http library
            throw new System.NotImplementedException();
        }

        public abstract File GetFile(string path);

        public abstract List<File> GetFiles(string folderPath);

        public abstract File AddFile(File file, string folderPath);

        public abstract Folder AddFolder(Folder folder, string folderPath);
    }
}
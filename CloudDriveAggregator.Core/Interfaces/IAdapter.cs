﻿using System.Collections.Generic;
using CloudDriveAggregator.Core.Models;

namespace CloudDriveAggregator.Core.Interfaces
{
    public interface IAdapter
    {
        File GetFileByUrl(string url); // todo: implement this one in base class, then inherit form base
        File GetFile(string path);
        List<File> GetFiles(string folderPath);
        File AddFile(File file, string folderPath); // returns new added file
        Folder AddFolder(Folder folder, string folderPath);
    }
}
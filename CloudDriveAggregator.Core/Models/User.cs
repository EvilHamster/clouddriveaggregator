﻿using System.Collections.Generic;
using CloudDriveAggregator.Core.Interfaces;

namespace CloudDriveAggregator.Core.Models
{
    public class User
    {
        public List<IAdapter> AvailableAdapters { get; set; }
    }
}
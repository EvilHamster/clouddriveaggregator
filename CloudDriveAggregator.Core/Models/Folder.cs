﻿using System.Collections.Generic;

namespace CloudDriveAggregator.Core.Models
{
    public class Folder : DiskData
    {
        public List<File> Files { get; set; }
    }
}
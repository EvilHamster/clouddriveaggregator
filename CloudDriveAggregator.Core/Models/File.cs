﻿using System.IO;

namespace CloudDriveAggregator.Core.Models
{
    public class File : DiskData
    {
        public FileType FileType { get; set; }
        public Stream ContentStream { get; set; } // dont know what you would use
        public byte[] ByteArray { get; set; } // keep only one
    }

    public enum FileType
    {
        Default // you can use here "document, picture, ..."
    }
}
﻿namespace CloudDriveAggregator.Core.Models
{
    public class DiskData
    {
        // original id
        public string Id { get; set; }
        // service data (atm only name, ex: google, yandex, onedrive, etc)
        public DataProvider DataProvider { get; set; }
        // File name
        public string Name { get; set; }
        // Internal id used in our system
        public string InternalId => $"{DataProvider.Name}:{Id}";
        // Folder location 
        public string Path { get; set; } // location with name
        // File location
        public string FullPath => $"{Path}\\{Name}"; // todo: overridable
        // Url to file (if possible)
        public string DataUrl { get; set; }
        // Total size in bytes
        public int Size { get; set; }
    }
}